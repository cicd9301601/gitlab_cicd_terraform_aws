
resource "aws_instance" "my_vm" {
  ami           = var.ami //Amazon Linux2 AMI
  instance_type = var.instance_type

  tags = {
    Name = var.name_tag,
    Created_by = var.created_by
  }
}
